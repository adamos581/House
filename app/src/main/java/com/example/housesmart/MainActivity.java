package com.example.housesmart;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.annotation.UiThread;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import com.example.housesmart.R;
import com.example.housesmart.activities.LoginActivity;
import com.example.housesmart.adapters.MainPageAdapter;
import com.example.housesmart.services.TCPService;
import com.orm.SchemaGenerator;
import com.orm.SugarContext;
import com.orm.SugarDb;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;


@EActivity(R.layout.activity_main)
public class MainActivity extends DrawerBaseActivity {

    MainPageAdapter mMainPageAdapter;
    ViewPager mViewPager;

    @ViewById(R.id.tab_layout)
    TabLayout tabLayout;

    public Socket socket;
    TCPService mService;


    private int[] tabIcons = {
            R.drawable.ic_image_wb_cloudy,
            R.drawable.ic_action_image_wb_incandescent
    };



    @AfterViews
    public void setupAfterView(){


        mMainPageAdapter = new MainPageAdapter(getSupportFragmentManager());
         mViewPager = (ViewPager) findViewById(R.id.mainViewPager);
        mViewPager.setAdapter(mMainPageAdapter);
        tabLayout.setupWithViewPager(mViewPager);
        setupTabIcons();
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[0]);
        tabLayout.getTabAt(2).setIcon(tabIcons[1]);
        tabLayout.getTabAt(3).setIcon(tabIcons[0]);
    }


}
