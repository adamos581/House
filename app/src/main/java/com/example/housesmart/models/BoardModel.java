package com.example.housesmart.models;

import android.graphics.Bitmap;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by adamo_000 on 19.03.2017.
 */

public class BoardModel
{
    private Calendar time;
    private String name;
    private String describe;
    private String type;

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    private Bitmap bitmap;


    public BoardModel(Calendar time, String name, String describe, String type) {
        this.time = time;
        this.name = name;
        this.describe = describe;
        this.type = type;
    }

    public Calendar getTime() {
        return time;
    }

    public void setTime(Calendar time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
