package com.example.housesmart.models;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by A637906 on 2016-09-12.
 */
public class DeviceModel extends SugarRecord {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String name;
    public String type;

    public DeviceModel(String name, String type) {
        this.name = name;
        this.type = type;
    }
    public DeviceModel(){}
}
