package com.example.housesmart.models;

import com.orm.SugarRecord;
import com.orm.dsl.Table;
import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

/**
 * Created by A637906 on 2016-09-09.
 */
@Parcel
public class LightingModel extends SugarRecord{

    private boolean condition;
    private String color;
    private String place;
    private String name;
    @ParcelConstructor
    public LightingModel(boolean condition, String color,String place, String name) {
        this.condition = condition;
        this.color = color;
        this.place = place;
        this.name = name;
    }

    public LightingModel(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isCondition() {
        return condition;
    }

    public void setCondition(boolean condition) {
        this.condition = condition;
    }
}
