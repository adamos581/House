package com.example.housesmart.models;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by adamo_000 on 12.11.2016.
 */

public class WeatherModel extends SugarRecord {
    private int temperature;
    private int wind;
    private int himidity;
    private int rain;

    public WeatherModel(int temperature, int wind, int himidity, int rain) {
        this.temperature = temperature;
        this.wind = wind;
        this.himidity = himidity;
        this.rain = rain;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getWind() {
        return wind;
    }

    public void setWind(int wind) {
        this.wind = wind;
    }

    public int getHimidity() {
        return himidity;
    }

    public void setHimidity(int himidity) {
        this.himidity = himidity;
    }

    public int getRain() {
        return rain;
    }

    public void setRain(int rain) {
        this.rain = rain;
    }
}
