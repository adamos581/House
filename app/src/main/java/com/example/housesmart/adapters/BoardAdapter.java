package com.example.housesmart.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.example.housesmart.holders.ViewCentralWrapper;
import com.example.housesmart.models.BoardModel;
import com.example.housesmart.view.BoardView;
import com.example.housesmart.view.BoardView_;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by adambelniak on 22.03.2017.
 */
@EBean
public class BoardAdapter extends RecyclerView.Adapter<ViewCentralWrapper<BoardView>> {
        @RootContext
        Context context;

        public void setItems(List<BoardModel> items) {
                this.items = items;
        }

        protected List<BoardModel> items = new ArrayList<BoardModel>();


        protected BoardView onCreateItemView(ViewGroup parent, int viewType) {
                return BoardView_.build(context);
        }

        @Override
        public final ViewCentralWrapper<BoardView> onCreateViewHolder(ViewGroup parent, int viewType) {
                return new ViewCentralWrapper<BoardView>(onCreateItemView(parent, viewType));
        }


        @Override
        public void onBindViewHolder(ViewCentralWrapper<BoardView> viewHolder, int position) {
                BoardView view = viewHolder.getView();
                BoardModel person = items.get(position);

        view.bind(person);
        }

        @Override
        public int getItemCount() {
                return items.size();
        }

        public void addToList(BoardModel deviceModel){
                items.add(0,deviceModel);
                this.notifyDataSetChanged();
        }
}


