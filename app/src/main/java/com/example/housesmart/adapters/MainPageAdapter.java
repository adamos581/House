package com.example.housesmart.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.housesmart.fragments.BoardFragment;
import com.example.housesmart.fragments.BoardFragment_;
import com.example.housesmart.fragments.EmptyFragment;
import com.example.housesmart.fragments.InfoMainFragment;
import com.example.housesmart.fragments.LightMainFragment;

/**
 * Created by A637906 on 2016-09-01.
 */
public class MainPageAdapter extends FragmentStatePagerAdapter {

    private final int NUM_ITEMS = 4;

    public MainPageAdapter(FragmentManager fm) {
        super(fm);
    }
     @Override
     public Fragment getItem(int index) {
         Fragment fragment =null;
         switch (index){

             case 0:fragment = BoardFragment.newInstance();

             case 1:fragment =  InfoMainFragment.newInstance();
                 break;
             case 2:
                 fragment =  LightMainFragment.newInstance();
                 break;
             case 3:
                 fragment =  EmptyFragment.newInstance();
                break;
         }
         return fragment;
     }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }
}


