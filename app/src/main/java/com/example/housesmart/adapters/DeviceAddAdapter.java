package com.example.housesmart.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.example.housesmart.holders.ViewDeviceWrapper;
import com.example.housesmart.models.DeviceModel;
import com.example.housesmart.view.DeviceItemsView;
import com.example.housesmart.view.DeviceItemsView_;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by A637906 on 2016-09-12.
 */
@EBean
public class DeviceAddAdapter extends RecyclerView.Adapter<ViewDeviceWrapper<DeviceItemsView>> {
     @RootContext
     Context context;

    public void setItems(List<DeviceModel> items) {
        this.items = items;
    }

    protected List<DeviceModel> items = new ArrayList<DeviceModel>();


    protected DeviceItemsView onCreateItemView(ViewGroup parent, int viewType) {
        return DeviceItemsView_.build(context);
    }

    @Override
    public final ViewDeviceWrapper<DeviceItemsView> onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewDeviceWrapper<DeviceItemsView>(onCreateItemView(parent, viewType));
    }


    @Override
    public void onBindViewHolder(ViewDeviceWrapper<DeviceItemsView> viewHolder, int position) {
        DeviceItemsView view = viewHolder.getView();
        DeviceModel person = items.get(position);

        view.bind(person);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void addToList(DeviceModel deviceModel){
        items.add(0,deviceModel);
        this.notifyDataSetChanged();
    }
}

