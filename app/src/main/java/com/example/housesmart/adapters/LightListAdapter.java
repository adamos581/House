package com.example.housesmart.adapters;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.housesmart.R;
import com.example.housesmart.listeners.LightListListener;
import com.example.housesmart.models.LightingModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by A637906 on 2016-09-09.
 */
public class LightListAdapter extends RecyclerView.Adapter<LightListAdapter.ViewHolder>{

    private List<LightingModel> modelRequestList = new ArrayList();
    private LightListListener toggleListener;


    public LightListAdapter(List<LightingModel>  modelRequestList,LightListListener toggleListener ){
        this.modelRequestList = modelRequestList;
        this.toggleListener = toggleListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.light_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final LightingModel modelRequest = modelRequestList.get(position);

        int imageID = modelRequest.isCondition() ? R.drawable.ic_light_on : R.drawable.ic_light_off ;
        holder.imageLight.setImageResource(imageID);
            //TODO
        holder.textViewItem.setText(modelRequest.getName());
        holder.colorInfo.setText((modelRequest.getColor()));

        holder.button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                modelRequest.setCondition(isChecked);
                toggleListener.recyclerViewToggleClicked(buttonView , position);
                LightListAdapter.this.notifyDataSetChanged();
            }
        });
    }
    @Override
    public int getItemCount() {
        return modelRequestList.size();
    }


    public  class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewItem;
        public ImageView imageLight;
        public TextView colorInfo;
        public SwitchCompat button;

        public ViewHolder(View v) {
                super(v);
                this.textViewItem = (TextView)v.findViewById(R.id.light_info);
                this.imageLight = (ImageView) v.findViewById(R.id.icon_light);
                this.colorInfo = (TextView)v.findViewById(R.id.color_info);
                this.button = (SwitchCompat)v.findViewById(R.id.light_switch);
            }
        }

    public void addToList(LightingModel lightingModel){
        modelRequestList.add(0,lightingModel);
        this.notifyDataSetChanged();
    }

}
