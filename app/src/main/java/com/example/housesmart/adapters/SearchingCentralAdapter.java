package com.example.housesmart.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.example.housesmart.holders.ViewCentralWrapper;
import com.example.housesmart.models.DeviceModel;
import com.example.housesmart.view.DeviceItemsView;
import com.example.housesmart.view.DeviceItemsView_;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by adamo_000 on 09.10.2016.
 */
@EBean
public class SearchingCentralAdapter extends RecyclerView.Adapter<ViewCentralWrapper<DeviceItemsView>> {
    @RootContext
    Context context;

    public void setItems(List<DeviceModel> items) {
        this.items = items;
    }

    protected List<DeviceModel> items = new ArrayList<DeviceModel>();


    protected DeviceItemsView onCreateItemView(ViewGroup parent, int viewType) {
        return DeviceItemsView_.build(context);
    }

    @Override
    public final ViewCentralWrapper<DeviceItemsView> onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewCentralWrapper<DeviceItemsView>(onCreateItemView(parent, viewType));
    }


    @Override
    public void onBindViewHolder(ViewCentralWrapper<DeviceItemsView> viewHolder, int position) {
        DeviceItemsView view = viewHolder.getView();
        DeviceModel person = items.get(position);

        view.bind(person);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void addToList(DeviceModel deviceModel){
        items.add(0,deviceModel);
        this.notifyDataSetChanged();
    }
}
