package com.example.housesmart;

/**
 * Created by adamo_000 on 03.10.2016.
 */
public final  class DataPassingMessages {

    // Defines a custom Intent action
    public static final String BROADCAST_ACTION =
            "BROADCAST";
    // Defines the key for the status "extra" in an Intent
    public static final String EXTENDED_DATA_STATUS =
            "STATUS";

}
