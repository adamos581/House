package com.example.housesmart.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.housesmart.BroadcastMesssages;
import com.example.housesmart.DataPassingMessages;
import com.example.housesmart.TCPHeadears;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by adamo_000 on 01.11.2016.
 */

public class TCPService extends Service {

    public static final String SERVERIP = "192.168.1.6"; //your computer IP address should be written here
    public static final int SERVERPORT = 4444;
    private final IBinder mBinder = new LocalBinder();
    private PrintWriter out;
    private BufferedReader in;
    private Socket socket;
    private InetAddress serverAddr;
    boolean mRun;
    public Receive listener;

    public class LocalBinder extends Binder {
       public  TCPService getService() {
            return TCPService .this;
        }
    }

    public void sendMessage(String message){
        if (out != null && !out.checkError()) {
            System.out.println("in sendMessage"+message);

            out.println(message);
            out.flush();
        }
    }

    @Override
    public int onStartCommand(Intent intent,int flags, int startId){
        super.onStartCommand(intent, flags, startId);
        System.out.println("I am in on start");
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (mWifi.isConnected()) {
            // Do whatever
        }


        Runnable connect = new connectSocket();
        new Thread(connect).start();
        return START_STICKY;
    }

    class connectSocket implements Runnable {
        @Override
        public void run() {
            try {
                serverAddr = InetAddress.getByName(SERVERIP);
                socket = new Socket(serverAddr, SERVERPORT);
                try {
                    //send the message to the server
                    out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
                    System.out.println("I am in on start");
                    in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    mRun=true;
                    while (mRun) {
                        if(out == null){
                            break;
                        }
                       String incomingMessage = in.readLine();
                        if (incomingMessage != null ) {
                            System.out.println(incomingMessage);
                            BroadcastInfo(incomingMessage);
                        }
                        incomingMessage = null;
                    }
                }
                catch (Exception e) {
                    Log.e("TCP", "S: Error", e);
                }
            } catch (Exception e) {
                Log.e("TCP", "C: Error", e);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {mRun = false;
            socket.close();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        socket = null;
    }


    protected void BroadcastInfo(String info){


        if(info.contains(TCPHeadears.REQUEST_DEVICE_ADD)){
            int size = TCPHeadears.REQUEST_DEVICE_ADD.length();
                String newMessage = info.substring(size);
            Intent localIntent =
                    new Intent(BroadcastMesssages.ADD_DEVICE_MESSEGE)
                            .putExtra(DataPassingMessages.EXTENDED_DATA_STATUS, newMessage);
            sendBroadcast(localIntent);
            }
        else{
            Intent localIntent =
                    new Intent(BroadcastMesssages.INFO_MAIN_MESSEGE)
                            .putExtra(DataPassingMessages.EXTENDED_DATA_STATUS, info);
            sendBroadcast(localIntent);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public void MessageCallback(String temperature) {
         listener.sendData(temperature );
    }
    public interface Receive{
        void sendData(String temperature);
    }
}
