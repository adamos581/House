package com.example.housesmart.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by A637906 on 2016-09-12.
 */

public class ViewDeviceWrapper<V extends View> extends RecyclerView.ViewHolder {

    private V view;

    public ViewDeviceWrapper(V itemView) {
        super(itemView);
        view = itemView;
    }

    public V getView() {
        return view;
       }
}
