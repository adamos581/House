package com.example.housesmart.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by adamo_000 on 09.10.2016.
 */
public class ViewCentralWrapper<V extends View> extends RecyclerView.ViewHolder {
    private V view;

    public ViewCentralWrapper(V itemView) {
        super(itemView);
        view = itemView;
    }

    public V getView() {
        return view;
    }
}
