package com.example.housesmart.fragments;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.os.IBinder;
import android.support.annotation.UiThread;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.housesmart.BroadcastMesssages;
import com.example.housesmart.DataPassingMessages;
import com.example.housesmart.MainActivity;
import com.example.housesmart.R;
import com.example.housesmart.TCPHeadears;
import com.example.housesmart.activities.AddDevices_;
import com.example.housesmart.activities.LoginActivity;
import com.example.housesmart.activities.SearchingCentralDevice_;
import com.example.housesmart.models.WeatherModel;
import com.example.housesmart.services.TCPService;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.EReceiver;
import org.androidannotations.annotations.Receiver;
import org.androidannotations.annotations.ViewById;

import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import static com.example.housesmart.DataPassingMessages.BROADCAST_ACTION;

/**
 * Created by A637906 on 2016-09-01.
 */


@EFragment(R.layout.main_activity_info)
public class InfoMainFragment extends Fragment implements TCPService.Receive {

    @ViewById(R.id.plus_main)
    Button plus;

    @ViewById(R.id.thermostat_value)
    TextView value;

    @ViewById(R.id.temperature_value)
    TextView temperatureV;

    @ViewById(R.id.rain_value)
    TextView rainV;

    @ViewById(R.id.humidy_value)
    TextView humidyV;

    @ViewById(R.id.wind_value)
    TextView windV;

    TCPService mService;
    private int temperature =20;
    private final double  deltaTemperature = 1;
    private final int lengthActionString = 4;

;

    @AfterViews
    public void setupAfterView(){
        value.setText("20" + (char) 0x00B0);
            doBindService();
        //WeatherModel weather = WeatherModel.findById(WeatherModel.class, 1);
    }



  @Receiver(actions = BroadcastMesssages.INFO_MAIN_MESSEGE)
    protected void onAction1(@Receiver.Extra(DataPassingMessages.EXTENDED_DATA_STATUS) String valueString) {

      if(valueString.length() > lengthActionString) {
          String action = valueString.substring(0, lengthActionString);
          String value = valueString.substring(lengthActionString);

          switch (action) {
              case TCPHeadears.BROADCAST_TEMP:
                  temperatureV.setText(value +  (char) 0x00B0);
                  ;
                  break;
              case TCPHeadears.BROADCAST_WIND:
                  windV.setText(value +"m/s");
                  ;
                  break;
              case TCPHeadears.BROADCAST_HUMID:
                  humidyV.setText(value + "%");
                  ;
                  break;
              case TCPHeadears.BROADCAST_RAIN:
                  rainV.setText(value + "mm");
                  ;
                  break;
          }
      }
    }


    @Click(R.id.plus_main)
    public void increaseTemperature(){
        mService.listener = this;
        temperature +=deltaTemperature;
        value.setText(String.valueOf(temperature) + (char) 0x00B0);
        mService.sendMessage(TCPHeadears.BROADCAST_TEMP + temperature );
    }

    @Click(R.id.minus_button)
    public void reduceTemperature(){
        mService.listener = this;
        temperature -= deltaTemperature;
        value.setText(String.valueOf(temperature) + (char) 0x00B0);
        mService.sendMessage(TCPHeadears.BROADCAST_TEMP + temperature );
    }

    public static InfoMainFragment newInstance() {
        InfoMainFragment singleListItemDisplayFragment = new InfoMainFragment_( );
        singleListItemDisplayFragment.setHasOptionsMenu(true);
        return singleListItemDisplayFragment;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE ) {
            ((LinearLayout)getView()).setOrientation((LinearLayout.HORIZONTAL));
        }
        else  if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT ) {
            ((LinearLayout)getView()).setOrientation((LinearLayout.VERTICAL));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.toolbar_settings_menu, menu);
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        //EDITED PART
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // TODO Auto-generated method stub
            mService = ((TCPService.LocalBinder)service).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // TODO Auto-generated method stub
            mService = null;
        }
    };

    private void doBindService() {
        getActivity().bindService(new Intent(getActivity(), TCPService.class), mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle item selection
        switch (item.getItemId()) {
            case R.id.add:
                AddDevices_.intent(this).start();
                return true;
            case R.id.find_house:
                SearchingCentralDevice_.intent(this).start();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void sendData(final String temperature) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                temperatureV.setText(temperature);
            }
        });
    }


}


