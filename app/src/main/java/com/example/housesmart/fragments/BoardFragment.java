package com.example.housesmart.fragments;

import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.housesmart.R;
import com.example.housesmart.adapters.BoardAdapter;
import com.example.housesmart.mocks.BoardInfoMock;
import com.example.housesmart.mocks.BoardRepository;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;

/**
 * Created by adamo_000 on 23.02.2017.
 */

@EFragment(R.layout.board_main)
public class BoardFragment extends Fragment {


    BoardRepository repository = new BoardInfoMock();
    @Bean
    BoardAdapter adapter;


    public static BoardFragment newInstance() {
        BoardFragment singleListItemDisplayFragment = new BoardFragment_( );
        singleListItemDisplayFragment.setHasOptionsMenu(true);
        return singleListItemDisplayFragment;
    }

    @AfterViews
    protected void setViews(){

        RecyclerView mRecyclerView = (RecyclerView)getView().findViewById(R.id.board_recycle_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(adapter);
    }

    @AfterInject
    protected void getFormDatabase(){
        adapter.addToList(repository.getFirstElement());

    }
}
