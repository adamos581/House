package com.example.housesmart.fragments;

/**
 * Created by A637906 on 2016-09-01.
 */

import android.support.v4.app.Fragment;
import com.example.housesmart.R;
import org.androidannotations.annotations.EFragment;


@EFragment(R.layout.empty_layout)
public class EmptyFragment extends Fragment {
    public static EmptyFragment newInstance() {
        EmptyFragment singleListItemDisplayFragment = new EmptyFragment_();
        return singleListItemDisplayFragment;
    }
}