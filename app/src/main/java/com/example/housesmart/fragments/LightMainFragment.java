package com.example.housesmart.fragments;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.housesmart.BroadcastMesssages;
import com.example.housesmart.R;
import com.example.housesmart.adapters.LightListAdapter;
import com.example.housesmart.listeners.LightListListener;
import com.example.housesmart.models.LightingModel;
import com.example.housesmart.services.TCPService;
import com.orm.SchemaGenerator;
import com.orm.SugarContext;
import com.orm.SugarDb;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.InstanceState;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by A637906 on 2016-09-08.
 */

@EFragment(R.layout.light_fragment)
public class LightMainFragment extends Fragment {



    @InstanceState
    protected List<LightingModel> modelArray = new ArrayList<>();
    TCPService mService ;

    private LightListAdapter mAdapter;


    @AfterViews
    public void after(){
        doBindService();
        RecyclerView mRecyclerView = (RecyclerView) getView().findViewById(R.id.list_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));


        if(0==(LightingModel.count(LightingModel.class ) )){

            (new LightingModel(false, "Red","kuchnia","Swiatlo 1")).save();
        }

        /*Bindujemy przed dodaniem widoku */
        setLightListener();
        mRecyclerView.setAdapter(mAdapter);
    }


    private void setLightListener() {
        mAdapter = new LightListAdapter(LightingModel.listAll(LightingModel.class), new LightListListener() {
            @Override
            public void recyclerViewToggleClicked(View v, int position) {
                LightingModel model = LightingModel.findById(LightingModel.class,position);
                if(model.isCondition()) {
                    mService.sendMessage(model.getName() + "1");
                }else{
                    mService.sendMessage(model.getName() + "0");
                }
            }
        });
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.toolbar_settings_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle item selection
        switch (item.getItemId()) {
            case R.id.add:
                return true;
            case R.id.find_house:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static LightMainFragment newInstance() {
        LightMainFragment singleListItemDisplayFragment = new LightMainFragment_();
        singleListItemDisplayFragment.setHasOptionsMenu(true);
        return singleListItemDisplayFragment;
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        //EDITED PART
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // TODO Auto-generated method stub
            mService = ((TCPService.LocalBinder)service).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // TODO Auto-generated method stub
            mService = null;
        }
    };

    private void doBindService() {
        getActivity().bindService(new Intent(getActivity(), TCPService.class), mConnection, Context.BIND_AUTO_CREATE);
    }
}
