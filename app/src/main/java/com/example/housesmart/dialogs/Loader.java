package com.example.housesmart.dialogs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;

/**
 * Created by adamo_000 on 09.10.2016.
 */
public class Loader {

    private static final String TAG = "Loader";

    private static ProgressDialog loader;

    public static void addLoader(Activity activity, String message) {
        if (loader == null || !loader.isShowing()) {
            if (!activity.isFinishing()) {
                try {
                    loader = new ProgressDialog(activity);
                    loader.setCancelable(false);
                    loader.setTitle("Please Wait...");
                    loader.setMessage(message);
                    loader.show();
                } catch (Exception ex) {
                    Log.e(TAG, "Problem while showing loader", ex);
                }
            }
        }
    }

    public static void hideLoader() {
        if (loader != null) {
            if (loader.isShowing()) {
                try {
                    loader.dismiss();
                } catch (IllegalArgumentException ex) {
                    Log.d(TAG, "Problem with dismiss loader", ex);
                }
            }
            loader = null;
        }
    }
}
