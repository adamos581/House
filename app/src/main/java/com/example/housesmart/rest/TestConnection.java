package com.example.housesmart.rest;

import android.util.Log;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.androidannotations.rest.spring.annotations.RestService;

/**
 * Created by adamo_000 on 31.03.2017.
 */
@EBean
public class TestConnection {

    private static final String TAG = "BackgroundConnection";

    public interface GetRequest {
        void success(UserModel user);
        void error();
    }

    @RestService
    public RestInterface restInterface;

    @Background
    public void downloadRequest (GetRequest myInterface)  {
        UserModel user ;
        try {
            user = restInterface.getObjectFromWeb();

            if(user != null) {
                myInterface.success(user);
            } else {
                myInterface.error();
            }
        }
        catch (Exception e){
            Log.e(TAG, "Error with download request", e);
            myInterface.error();
        }
    }
}
