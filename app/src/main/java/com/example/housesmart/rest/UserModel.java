package com.example.housesmart.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.parceler.Parcel;

/**
 * Created by adamo_000 on 31.03.2017.
 */
@Parcel
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserModel {
    private String login;
    private String email;
    private String passwordHash;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }
    public String asString(){
        return new StringBuffer().append("Login:").append(login)
                .append("\norigin: " + email)
                .append("\nurl: " + passwordHash).toString();
    }
}
