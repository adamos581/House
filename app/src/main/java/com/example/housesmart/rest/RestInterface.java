package com.example.housesmart.rest;

import org.androidannotations.rest.spring.annotations.Accept;
import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.api.MediaType;
import org.androidannotations.rest.spring.api.RestClientErrorHandling;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

/**
 * Created by adamo_000 on 31.03.2017.
 */

//do zmiany poniewaz zalezny od adresu w sieci lokalnej
@Rest(rootUrl = "192.168.1.4" , converters = {MappingJackson2HttpMessageConverter.class })
public interface RestInterface extends RestClientErrorHandling {
    @Get("/get")
    @Accept(MediaType.APPLICATION_JSON)
    UserModel getObjectFromWeb();
}
