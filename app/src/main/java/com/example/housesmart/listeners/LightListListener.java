package com.example.housesmart.listeners;

import android.view.View;

/**
 * Created by A637906 on 2016-09-12.
 */
public interface LightListListener {
    void recyclerViewToggleClicked(View v, int position);

}
