package com.example.housesmart;

/**
 * Created by adamo_000 on 10.02.2017.
 */

public class TCPHeadears {

    public static final String REQUEST_DEVICE_ADD = "GET_DEVICE_LIST";
    public static final String BROADCAST_TEMP = "TEMP";
    public static final String BROADCAST_WIND = "WIND";
    public static final String BROADCAST_HUMID = "HUMD";
    public static final String BROADCAST_RAIN = "RAIN";
}
