package com.example.housesmart.view;

        import com.example.housesmart.R;

        import org.androidannotations.annotations.EBean;

        import java.util.HashMap;
        import java.util.Map;

/**
 * Created by adambelniak on 24.03.2017.
 */
@EBean
public class Icon_contener {

    private  Map<String, Integer> icon = new HashMap<String, Integer>();

    public Icon_contener(){
        icon.put("device", R.drawable.ic_action_action_settings);
    }

    public int getDrawableIcon(String name) {
        return icon.get("device");
    }
}
