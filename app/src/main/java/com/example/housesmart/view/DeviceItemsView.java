package com.example.housesmart.view;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.housesmart.R;
import com.example.housesmart.models.DeviceModel;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by A637906 on 2016-09-12.
 */
@EViewGroup(R.layout.device_list_view)
public class DeviceItemsView extends LinearLayout {

    @ViewById(R.id.device_name)
    TextView name;

    @ViewById(R.id.device_type)
    TextView type;

    public DeviceItemsView(Context context) {
        super(context);
    }

    public void bind(DeviceModel device) {
        name.setText(device.name);
        type.setText(device.type);
    }
}

