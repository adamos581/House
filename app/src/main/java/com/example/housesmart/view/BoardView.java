package com.example.housesmart.view;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.housesmart.R;
import com.example.housesmart.models.BoardModel;
import com.example.housesmart.models.DeviceModel;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by adambelniak on 22.03.2017.
 */
@EViewGroup(R.layout.board_list_item)
public class BoardView extends LinearLayout {

    @ViewById(R.id.topic)
    TextView topic;

    @ViewById(R.id.describe)
    TextView describe;

    @ViewById(R.id.image_board)
    ImageView image;

    @Bean
    Icon_contener icons;


    public BoardView(Context context) {
        super(context);
    }

    public void bind(BoardModel device) {
        topic.setText(device.getName());
        describe.setText(device.getDescribe());
        if(device.getBitmap() != null) {
            image.setImageBitmap(device.getBitmap());
        }else{
                Integer drawable = icons.getDrawableIcon(device.getType());
            if(drawable != null){
                image.setImageResource(drawable);
            }else{
                image.setImageResource(R.drawable.circle_photo);
            }
        }
    }
}
