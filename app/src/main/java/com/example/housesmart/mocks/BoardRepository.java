package com.example.housesmart.mocks;

import com.example.housesmart.models.BoardModel;

import java.util.Date;
import java.util.List;

/**
 * Created by adamo_000 on 19.03.2017.
 */

public interface BoardRepository {

    public List<BoardModel> getAllBoardInfo();
    public List<BoardModel> getSelectedInfo(Date start, Date finish);
    public List<BoardModel> getSelectedInfo(int start, int finish);
    public BoardModel getFirstElement();

}
