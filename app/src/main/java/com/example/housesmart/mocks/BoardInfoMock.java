package com.example.housesmart.mocks;

import com.example.housesmart.models.BoardModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by adamo_000 on 19.03.2017.
 */

public class BoardInfoMock implements  BoardRepository {


    List<BoardModel> database = new ArrayList<BoardModel>();

    public BoardInfoMock() {

        BoardModel model1 = new BoardModel(new GregorianCalendar(2017,1,13),"światło 1","światło zostało właczone","światło");
        database.add(model1);
    }


    @Override
    public List<BoardModel> getAllBoardInfo() {
        return null;
    }

    @Override
    public List<BoardModel> getSelectedInfo(Date start, Date finish) {
        return null;
    }

    @Override
    public List<BoardModel> getSelectedInfo(int start, int finish) {
        return null;
    }

    @Override
    public BoardModel getFirstElement() {
        return database.get(0);
    }
}
