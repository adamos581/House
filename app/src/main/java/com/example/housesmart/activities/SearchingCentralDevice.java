package com.example.housesmart.activities;

import org.androidannotations.annotations.UiThread;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.housesmart.R;
import com.example.housesmart.adapters.DeviceAddAdapter;
import com.example.housesmart.adapters.SearchingCentralAdapter;
import com.example.housesmart.dialogs.Loader;
import com.example.housesmart.models.DeviceModel;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by adamo_000 on 09.10.2016.
 */
@EActivity(R.layout.searching_central_list)

public class SearchingCentralDevice extends AppCompatActivity{
    @Bean
    SearchingCentralAdapter adapter;

    protected List<DeviceModel> modelArray = new ArrayList<>();

    @AfterViews
    public void createAfter(){
        RecyclerView mRecyclerView = (RecyclerView)findViewById(R.id.searching_central_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(adapter);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
        Loader.addLoader(this, "Szukanie urządzeń");
        searchingHost();

    }

    @Background
    protected void searchingHost(){
        int timeout=300;
        try {
            for (int i=1;i<100;i++){
                String host="192.168.1" + "." + i;
                if (InetAddress.getByName(host).isReachable(timeout)){
                    System.out.println(host + " is reachable");
                    modelArray.add(new  DeviceModel("Device", host));
                }
            }
        } catch (UnknownHostException e1) {
            e1.printStackTrace();
            System.out.print(e1);
        } catch (IOException e1) {
            e1.printStackTrace();
            System.out.print(e1);

        }
        addDevice();
    }

    @UiThread
    void addDevice(){
        Loader.hideLoader();
        for (DeviceModel model:modelArray
             ) {
            adapter.addToList( model);
        }
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE && newConfig.screenWidthDp > 820) {
            this.finish();
        }
    }

}
