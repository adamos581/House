package com.example.housesmart.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.annotation.UiThread;
import android.support.v7.app.AppCompatActivity;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.example.housesmart.MainActivity_;
import com.example.housesmart.R;
import com.example.housesmart.dialogs.Loader;
import com.example.housesmart.services.TCPService;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * Created by adamo_000 on 22.10.2016.
 */
@EActivity(R.layout.login_activity)
public class LoginActivity extends AppCompatActivity {


    @ViewById(R.id.email)
    protected AutoCompleteTextView mEmailView;
    @ViewById(R.id.password)
    protected EditText mPasswordView;

    TCPService mService;


    @AfterViews public void polaczenie(){
        startService(new Intent(LoginActivity.this,TCPService.class));
        doBindService();
    }

    @Click(R.id.email_sign_in_button)
    protected void sendLogin(){
        Loader.addLoader(this, (getString(R.string.logowanie)).toString());
        backgroundLog();
    }

    @Background(delay = 2000)
    protected void backgroundLog(){
      startApp();
    }

    @UiThread
    protected void startApp(){
        Loader.hideLoader();
        MainActivity_.intent(this).start();
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        //EDITED PART
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // TODO Auto-generated method stub
            mService = ((TCPService.LocalBinder)service).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // TODO Auto-generated method stub
            mService = null;
        }

    };

    private void doBindService() {
        bindService(new Intent(LoginActivity.this, TCPService.class), mConnection, Context.BIND_AUTO_CREATE);
    }

}
