package com.example.housesmart.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.annotation.UiThread;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.housesmart.BroadcastMesssages;
import com.example.housesmart.DataPassingMessages;
import com.example.housesmart.DrawerBaseActivity;
import com.example.housesmart.R;
import com.example.housesmart.TCPHeadears;
import com.example.housesmart.adapters.DeviceAddAdapter;
import com.example.housesmart.adapters.LightListAdapter;
import com.example.housesmart.listeners.LightListListener;
import com.example.housesmart.models.DeviceModel;
import com.example.housesmart.models.LightingModel;
import com.example.housesmart.services.TCPService;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.Receiver;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by A637906 on 2016-09-12.
 */
@EActivity(R.layout.add_devices_activity)
public class AddDevices extends DrawerBaseActivity {

    @Bean
    DeviceAddAdapter adapter;
    TCPService mService;

    protected List<DeviceModel> modelArray = new ArrayList<>();

    @AfterViews
    public void createAfter(){
        RecyclerView mRecyclerView = (RecyclerView)findViewById(R.id.add_device_recycle);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(adapter);
        doBindService();
        DeviceModel.deleteAll(DeviceModel.class);
        modelArray = DeviceModel.listAll(DeviceModel.class);

            for(int k = 0; k < 3 ; k++) {
                DeviceModel model = new DeviceModel("Urządzenie 1", "Swiatlo 1");
                model.save();
                adapter.addToList(model);
            }
        if(mService != null) {
            sendRequest();
        }
    }

    private void sendRequest(){
        mService.sendMessage(TCPHeadears.REQUEST_DEVICE_ADD);
    }



    @Receiver(actions = BroadcastMesssages.ADD_DEVICE_MESSEGE)
    protected void onAction1(@Receiver.Extra(DataPassingMessages.EXTENDED_DATA_STATUS) String valueString) {

        //kolejne informacje o urządeniu są oddzielone kropką
        String[] parts = valueString.split("\\.");
        if (parts.length > 1){
            DeviceModel model = new DeviceModel(parts[0], parts[1]);
            model.save();
            adapter.addToList(model);
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        //EDITED PART
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // TODO Auto-generated method stub
            mService = ((TCPService.LocalBinder)service).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // TODO Auto-generated method stub
            mService = null;
        }
    };

    private void doBindService() {
        bindService(new Intent(this, TCPService.class), mConnection, Context.BIND_AUTO_CREATE);
    }

}
